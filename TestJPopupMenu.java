import java.awt.Color; 
import java.awt.event.ActionEvent; 
import java.awt.event.ActionListener; 
import java.awt.event.MouseAdapter; 
import java.awt.event.MouseEvent; 

import javax.swing.JFrame; 
import javax.swing.JMenuItem; 
import javax.swing.JPopupMenu; 
 
public class TestJPopupMenu extends JFrame { 
    private JPopupMenu popupMenu; 
    private JMenuItem items[]; 
 
    Color[] colors = { Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW, Color.PINK }; 
    public TestJPopupMenu() { 
        popupMenu = new JPopupMenu(); 
        String[] str = { "Merah", "hijau", "biru", "kuning", "merah muda" }; 
        items = new JMenuItem[5];  
        MenuItemMonitor menuItemMonitor = new MenuItemMonitor(); 
        for (int i = 0; i < items.length; i++) { 
            items[i] = new JMenuItem(str[i]); 
            popupMenu.add(items[i]); 
            items[i].setActionCommand(i + ""); 
            items[i].addActionListener(menuItemMonitor); 
        } 
        addMouseListener(new MouseAdapter() { 
            public void mouseReleased(MouseEvent event) {
                if (event.isPopupTrigger()) 
                    popupMenu.show(event.getComponent(), event.getX(), event.getY()); 
            } 
        }); 
        getContentPane().setColor(Color.white); 
        setTitle("Klik kanan menu pop-up"); 
        setBounds(200, 200, 400, 300); 
        setVisible(true); 
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
    } 
 
    private class MenuItemMonitor implements ActionListener { 
        @Override 
        public void actionPerformed(ActionEvent event) { 
            String strIndex = ((JMenuItem) event.getSource()).getActionCommand(); 
            int niIndex = Integer.parseInt(strIndex);  
            
        } 
    } 
 
    public static void main(String[] args) { 
        new TestJPopupMenu(); 
    } 
}